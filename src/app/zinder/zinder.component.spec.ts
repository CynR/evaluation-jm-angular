import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZinderComponent } from './zinder.component';

describe('ZinderComponent', () => {
  let component: ZinderComponent;
  let fixture: ComponentFixture<ZinderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZinderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
