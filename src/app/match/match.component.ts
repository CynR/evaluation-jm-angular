import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Match} from '../model/match';
import {NewMatch} from '../model/newMatch';
import {ApiZinderService} from '../service/apiZinder.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  @Output() match = new EventEmitter<Match>();
  @Input() listToMatch;
  matchCreated: NewMatch;
  @Input() idProfil = '';
  like = true;
  nope = false;


  constructor(
    private apiZinderService: ApiZinderService
  ) {
  }

  likeMatch(isMatch: boolean) {
    if (isMatch === this.like) {
      this.matchCreated = new NewMatch(this.like);
      this.apiZinderService.createMatch(this.idProfil, this.matchCreated).subscribe();
      console.log(this.idProfil + ' ' + this.matchCreated.match);
    } else {
      this.matchCreated = new NewMatch(this.nope);
      this.apiZinderService.createMatch(this.idProfil, this.matchCreated).subscribe();
      console.log(this.idProfil + ' ' + this.matchCreated.match);
    }
  }

  ngOnInit() {
  }

}
