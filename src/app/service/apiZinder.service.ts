import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Profil} from '../model/profil';
import {map} from 'rxjs/operators';
import {ListProfils} from '../model/list-profils';
import {Match} from '../model/match';
import {NewMatch} from '../model/newMatch';
import {NewProfile} from '../model/newProfile';


@Injectable()
export class ApiZinderService {

  constructor(
    private http: HttpClient
  ) {
  }

  getAllProfils(): Observable<Profil[]> {
    return this.http.get<ListProfils>('http://localhost:8088/zinder/profils').pipe(
      map(listProfils => listProfils.profils)
    );
  }

  createMatch(idProfil: string, newMatch: NewMatch): Observable<NewMatch> {
    return this.http.post<NewMatch>('http://localhost:8088/zinder/profils/' + idProfil + '/match', newMatch);
  }

  getAllMatchs(): Observable<Match[]> {
    return this.http.get<Match[]>('http://localhost:8088/zinder/matchs');
  }

    createProfil(newProfil: NewProfile): Observable<NewProfile> {
      return this.http.post<NewProfile>('http://localhost:8088/zinder/profils', newProfil);
    }


}
