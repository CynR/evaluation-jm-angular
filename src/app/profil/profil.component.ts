import {Component, Input, OnInit} from '@angular/core';
import {Profil} from '../model/profil';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  @Input() profil: Profil;

  constructor() {
  }

  createMatch(value: boolean) {

  }

  ngOnInit() {
  }

}
