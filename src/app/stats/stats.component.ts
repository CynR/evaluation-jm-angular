import {Component, OnInit} from '@angular/core';
import {ApiZinderService} from '../service/apiZinder.service';
import {Match} from '../model/match';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  listMatch: Match[];

  constructor(
    private apiZinderService: ApiZinderService
  ) {
  }


  ngOnInit() {
    this.apiZinderService.getAllMatchs().subscribe(reponse => {
      this.listMatch = reponse;
    });
  }

}
