import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Profil} from '../model/profil';
import {ApiZinderService} from '../service/apiZinder.service';
import {isBoolean} from 'util';
import {Match} from '../model/match';

@Component({
  selector: 'app-list-profils',
  templateUrl: './list-profils.component.html',
  styleUrls: ['./list-profils.component.css']
})
export class ListProfilsComponent implements OnInit {

  @Output() listProfils: Profil[];


  constructor(
    private apiZinderService: ApiZinderService
  ) {
  }



  ngOnInit() {
    this.apiZinderService.getAllProfils().subscribe(reponse => {
      this.listProfils = reponse;
    });
  }

}
