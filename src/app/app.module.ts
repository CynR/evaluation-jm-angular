import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { ZinderComponent } from './zinder/zinder.component';
import { StatsComponent } from './stats/stats.component';
import { ProfilComponent } from './profil/profil.component';
import { MatchComponent } from './match/match.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiZinderService} from './service/apiZinder.service';
import { ListProfilsComponent } from './list-profils/list-profils.component';
import { InteretsComponent } from './interets/interets.component';
import { AdministrationComponent } from './administration/administration.component';

const appRoutes: Routes = [
  { path: 'zinder', component: ZinderComponent},
  { path: 'stats', component: StatsComponent},
  { path: 'admin', component: AdministrationComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ZinderComponent,
    StatsComponent,
    ProfilComponent,
    MatchComponent,
    ListProfilsComponent,
    InteretsComponent,
    AdministrationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ApiZinderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
