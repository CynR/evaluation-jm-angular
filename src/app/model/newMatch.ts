export class NewMatch {
  match: boolean;

  constructor(match: boolean) {
    this.match = match;
  }
}
