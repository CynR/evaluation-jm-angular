import {Injectable} from '@angular/core';
import {Profil} from './profil';

@Injectable()
export class ListProfils {

  profils: Profil[];

}
