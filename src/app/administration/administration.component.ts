import {Component, OnInit} from '@angular/core';
import {NewProfile} from '../model/newProfile';
import {ApiZinderService} from '../service/apiZinder.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {

  nom: string;
  prenom: string;
  photoUrl: string;
  interets: string[];

  constructor(
    private apiZinderService: ApiZinderService
  ) {
  }

  validation() {
    const myProf = new NewProfile(this.prenom, this.nom, this.photoUrl, this.interets);
    this.apiZinderService.createProfil(myProf).subscribe();
  }

  ngOnInit() {
  }

}
